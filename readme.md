# Welcome to Challenge Ideas!
This repository is meant to be used, as a way to discuss ideas with the challenge team, where we can discuss, tweak and perfect them!
It is also a source of support, and should at a future point provide a array of tools that can be used for developing challenges!

At the moment we will however settle on a thinly made table of content, on what should be provided in the future!

# Submitting challenge ideas!
To submit a challenge idea for review/discussing/tweaking simply create a new issue in the format of `CATEGORY: Title of the challenge`.
Next select the default template, and start filling out the fields which applies to your challenge idea!

It is important to have the following in mind, when having a challenge idea:

- What is the goal of this challenge?
    - What should someone who completes the challenge learn?
- What skills does this challenge require?
- Who is the target audience of this challenge?
    - Education, job, age, novice, medium, hard, etc.
    - e.g. preschool, high school, college, in-job, etc.
- Can I make this challenge more simple (could I compile this challenge further down)?
    - Maybe it could be split into several smaller challenges instead?
- What value does this challenge provide compared to other challenges like this?

# Developing a challenge (from getting the idea, to deploying it)

## Idea Phase
Brain storm various security problems you might have experienced or know of!
Could also be things that you find important, and would like to teach others.
Keep in mind, what is obivious to you might not be for someone else!

Pinpoint the idea (or set of ideas), that have you either think makes the most sense, or have the most material on it!
 Take this idea and look at the above points, and see if it still is a good idea!

## Developing the idea!
Firstly plan out the idea, does it require any programming?
 Can you make it using simple shell commands, etc.?

Develop the code/challenge, and keep in mind, that someone in the future might have to maintain/edit the stuff that you make at the moment.
But do not let it hinder your process in any way, where you might try to perfect it, is that is NOT the goal, we are hacking and just want to get something to work!

## Making the challenge

## Testing the challeng:s

## Package the challenge!
TODO Insert a guide on how to do this here

