<!--
Please read this!

Ensure that you have set your issue to be confidential, as it should only be visible to you and the team!
If you do not do this, someone might be able to snatch you idea, or abuse the information you submit in this issue when it comes live!

Please read the readme file in the root of this repository before you submit a issue, for our considerations list!
-->

### Summary
<!-- This challenge has a webpage, where the goal is to edit a cookie to get admin access! -->

### What is the teaching goals for this challenge?
<!-- The ideas is to teach developers that cookies are not always secure, and to teach users what cookies are and how they can be abused -->

### Do we already provide teaching materials on this topic? Or do you plan on making any on the topic?
<!-- Yes, there is a tutorial on this topic [here)(some-link-a-website-under-octp.md) (this is not currently active) -->
<!-- or, no I intend to provide some teaching materials on this topic -->

### If yes regarding teaching material, how do you intend to structure it?
<!--
1. Simple introduction to what cookies are
2. How does the server/client handle cookies?
3. Where are they stored

I plan on this "course" taking approx 30 min to complete
-->

### Is there anything that you need us to do, for you to complete this?
<!-- Yes, I would like some sparing/ideas on this topic, maybe you know a good name for the admin we infiltrate! -->

### Is it OK for this issue + the challenge to be made public at a given time?
<!-- Keep in mind, that we will ping this issue but if no reply has been made in 14 days, we will publish it --> 
